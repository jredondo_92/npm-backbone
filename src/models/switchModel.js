import Bb from 'backbone'
import Radio from 'backbone.radio'

const SwitchModel = Bb.Model.extend({
    defaults: {
        grid: true,
    },

    initialize (options) {
        const channel = Radio.channel('app')
        this.listenTo(channel, 'update:switchView', (viewState) => this.onViewSwitch(viewState))
    },

    onViewSwitch(viewState){
        this.set("grid",viewState)
    }

})

export default SwitchModel