import Bb from 'backbone'
import Radio from 'backbone.radio'

const PaginatorModel = Bb.Model.extend({
    defaults: {
        actual: 1,
        last: 1,
    },

    initialize (options) {
        const channel = Radio.channel('app')
        this.listenTo(channel, 'update:currentPage', (options) => this.onPageUpdate(options))
    },

    onPageUpdate(options){
        this.set("actual",options.actual)
        this.set("last",options.last)
    }

})

export default PaginatorModel