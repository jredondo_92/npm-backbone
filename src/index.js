import _ from 'lodash';
import $ from 'jquery'
import json2 from 'json2js'
import Bb from 'backbone'
import Radio from 'backbone.radio'
import Mn from 'backbone.marionette'
import './resources/style.scss'
import MainView from './views/mainView'
import AppChannel from './services/appService'

const App = Mn.Application.extend({
    region: '#app',

    onStart() {
        const appChannel = new AppChannel()
        const mainView = new MainView()
        this.showView(mainView)
    }
})

    const app = new App()
    app.start()