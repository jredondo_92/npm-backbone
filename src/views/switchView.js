import Mn from 'backbone.marionette'
import SwitchTemplate from '../templates/switch.hbs'
import Radio from 'backbone.radio'
import _ from 'lodash'

const channel = Radio.channel('app')

const SwitchView = Mn.View.extend({
    template: SwitchTemplate,
    className: 'switch-cnt',
    ui: {
        switchInput: '.slider.round'
    },
    events: {
        "click @ui.switchInput" : "onSwitchSwitch"
    },

    initialize () {
        this.grid = false;
    },

    onSwitchSwitch(){
        this.grid = !this.grid
        channel.trigger('update:switchView', this.grid)
    }
})

export default SwitchView