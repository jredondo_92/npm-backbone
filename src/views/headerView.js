import Mn from 'backbone.marionette'
import HeaderTemplate from '../templates/header.hbs'

const HeaderView = Mn.View.extend({
    template: HeaderTemplate,
    tagName: 'header',

    // serializeData() {

    // }
/*     initialize () {
        this.model.fetch()
    } */

})

export default HeaderView