import Mn from 'backbone.marionette'
import AlbumTemplate from '../templates/album.hbs'

const AlbumView = Mn.View.extend({
    template: AlbumTemplate,
    className: 'item',

    // serializeData() {

    // }
/*     initialize () {
        this.model.fetch()
    } */

})

export default AlbumView