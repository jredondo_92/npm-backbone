import Mn from 'backbone.marionette'
import AlbumView from './albumView'
import Radio from 'backbone.radio'

const AlbumsCollectionView = Mn.CollectionView.extend({
    childView: AlbumView,
    id: "albumsList",
    className: "albumsCont grid-group-wrapper hidden",

    modelEvents: {
        'filtered': 'onFilteredCollection'
    },

    onRender: function() {
        if (this.collection.length > 1) this.$el.removeClass('hidden')
    },

    initialize () {
        const channel = Radio.channel('app')
        this.listenTo(channel, 'update:switchView', (viewState) => this.onViewSwitch(viewState))
    },

    onViewSwitch(viewState) {
        console.log(viewState)
        this.$el.toggleClass('grid-group-wrapper list-group-wrapper');
    }   
});

export default AlbumsCollectionView