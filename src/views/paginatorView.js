import Mn from 'backbone.marionette'
import PaginatorTemplate from '../templates/paginator.hbs'
import Radio from 'backbone.radio'

const channel = Radio.channel('app')

const PaginatorView = Mn.View.extend({
    template: PaginatorTemplate,
    className: 'paginator',
    ui: {
        actualPg: '.actual',
        lastPg: '.last',
        prevBtn: '.prev',
        nextBtn: '.next'
    },
    events: {
        "click @ui.prevBtn": "PreviousPage",
        "click @ui.nextBtn": "NextPage",
    },
    modelEvents: {
        "change" : "render"
    },

    initialize () {
    },

    PreviousPage () {
        let currentPage = this.model.get('actual')
        if (currentPage > 1){
            currentPage -= 1
            channel.trigger('filter:page', currentPage)
            channel.trigger('update:currentPage', {actual:currentPage, last: this.model.get('last')})
        }
    },
    
    NextPage () {
        let currentPage = this.model.get('actual')
        if (currentPage < this.model.get('last')){
            currentPage += 1
            channel.trigger('filter:page', currentPage)
            channel.trigger('update:currentPage', {actual:currentPage, last: this.model.get('last')})
        }
    }
})

export default PaginatorView