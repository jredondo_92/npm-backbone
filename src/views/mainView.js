import Mn from 'backbone.marionette'
import MainTemplate from '../templates/main.hbs'
import AlbumsCollectionView from './albumCollectionView'
import AlbumsCollection from '../collections/albumCollection'
import HeaderView from './headerView'
import SearchView from './searchView'
import SwitchView from './switchView'
import PaginatorView from './paginatorView'
import Radio from 'backbone.radio'
import PaginatorModel from '../models/paginatorModel';
import _ from 'lodash'
import SwitchModel from '../models/switchModel';

const channel = Radio.channel('app')


const MainView = Mn.View.extend({
    regions: {
        list: '.list-container',
        search: '.search-container',
        switch: '.switch-container',
        header: '.header-container',
        paginator: ".paginator-container",
    },
    template: MainTemplate,

    // serializeData() {

    // }
/*     initialize () {
        this.model.fetch()
    } */
    initialize (options) {
        this.listenTo(channel, 'updateSearch', (term) => this.onSearchUpdate(term))
        //this.listenTo(channel, 'update:currentPage', (options) => this.onPageUpdate(options))
    },
    
    onSearchUpdate (term) {
        if (!_.isEmpty(term)){
            const albumsList = new AlbumsCollectionView({         
                collection: new AlbumsCollection({searchTerm : term })
            })
            this.showChildView('list', albumsList)
        }
    },

    onRender () {
        const albumsList = new AlbumsCollectionView({         
            collection: new AlbumsCollection({searchTerm : 'Nice People' })
        })
        this.showChildView('header', new HeaderView({}));
        this.showChildView('paginator', new PaginatorView({model: new PaginatorModel()}));
        this.showChildView('search', new SearchView({}));
        this.showChildView('switch', new SwitchView({model: new SwitchModel()}));
        this.showChildView('list', albumsList)
    }
})

export default MainView