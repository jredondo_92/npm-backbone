import Mn from 'backbone.marionette'
import SearchTemplate from '../templates/search.hbs'
import Radio from 'backbone.radio'
import _ from 'lodash'

const channel = Radio.channel('app')

const SearchView = Mn.View.extend({
    template: SearchTemplate,
    className: 'search',
    ui: {
        searchInput: '#searchInput'
    },
    events: {
        "keyup @ui.searchInput" : "onKeyUpEvent"
    },

    initialize () {
        this.delayed = _.debounce(()=>{this.doSearch(this.ui.searchInput.val())},300)
    },

    onKeyUpEvent () {
       this.delayed()
    },

    doSearch (searchTerm) {
        channel.trigger('updateSearch', searchTerm)
    }
})

export default SearchView