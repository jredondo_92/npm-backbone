import Bb from 'backbone'
import AlbumModel from '../models/albumModel'
import Radio from 'backbone.radio'
import _ from 'lodash'


const app = 25 //albums per page

const AlbumsCollection = Bb.Collection.extend({
    model: AlbumModel,
    url: 'https://itunes.apple.com/search?entity=album&limit=200&term=',

    //Parse the response
    parse: function (data) {
        const albumData = this._transformAlbum(data, 0, 200);

        const count = (data.resultCount > 0 ? data.resultCount : 1);
        const pages = parseInt(count % app !== 0 ? count / app + 1 : count / app, 10);

        this.pageCount = pages

        return albumData;     
    },
    
    initialize (options) {
        this.url += options.searchTerm
        this.fetch()
            .then( (data) => {
                this.allModels = _.cloneDeep(this)
                this.onFilterPage(1)
                channel.trigger('update:currentPage', {actual:1, last: this.pageCount})
            })
        const channel = Radio.channel('app')
        this.listenTo(channel, 'filter:page', (page) => this.onFilterPage(page))
    },
    
    onFilterPage(page) {
        const to = page * app - 1;   
        const from = (to - (app -1 ));

        this.models = this.allModels.filter((model, i)=> {
            if (i>=from && i <= to) {
                return model
            }
        })
        this.reset(this.models)
    },

    _transformAlbum (data, from, to) {
        return data.results.filter((item,i) => i >= from && i <= to).map(item=>(
            {
                author: item.artistName,
                artwork: item.artworkUrl100,
                big_artwork: item.artworkUrl100.replace('100x100','200x200'),
                album: item.collectionName,
            }
        )) 
    }
})

export default AlbumsCollection