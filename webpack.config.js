const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const DashboardPlugin = require('webpack-dashboard/plugin')


module.exports = {
    "mode": "development",
    "entry": "./src/index.js",
    "output": {
        "path": __dirname+'/dist',
        "filename": "[name].[chunkhash:8].js"
    },
    "module": {
        "rules": [
            {
                "test": /\.scss$/,
                "use": [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }, {
                "test": /\.hbs$/,
                "use": [
                  {
                    loader: 'handlebars-loader',
                    query: {
                      helperDirs: [
                        // project.paths.src('helpers/handlebars')
                      ]
                    }
                  }
                ]
              }
        ]
    },
    "plugins": [ 
                new DashboardPlugin(),
                new HtmlWebpackPlugin({title: 'MyApp', filename: 'index.html', template: 'src/index.html'})
            ]
}



